# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""

# added by me to exit out of print
import sys

import gym
import math
import random
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
from collections import namedtuple
from itertools import count
from PIL import Image

import torch
import torch.nn as nn
import torch.optim as optim
import torch.nn.functional as F
import torchvision.transforms as T

# added by me to show entire numpy array
#np.set_printoptions(threshold=sys.maxsize)


env = gym.make('CartPole-v0').unwrapped

is_ipython = 'inline' in matplotlib.get_backend()
if is_ipython:
    from IPython import display
    
plt.ion()
device = torch.device('cuda' if torch.cuda.is_available() else "cpu")

Transition = namedtuple('Transition',
                        ('state', 'action', 'next_state', 'reward'))

class ReplayMemory(object):
    def __init__(self, capacity):
        self.capacity = capacity
        self.memory = []
        self.position = 0
        
    def push(self, *args):
        if len(self.memory) < self.capacity:
            self.memory.append(None)
        self.memory[self.position] = Transition(*args)
        self.position = (self.position + 1) % self.capacity
    
    def sample(self, batch_size):
        return random.sample(self.memory, batch_size)
    
    def __len__(self):
        return len(self.memory)
    
class DQN(nn.Module):
    def __init__(self, h, w, outputs):
        super(DQN, self).__init__()
        #Conv2d is calling Functional (F) F.conv2d(input_value)
        #https://pytorch.org/docs/stable/nn.functional.html#torch.nn.functional.conv2d
        #F.conv2d(inputs, filters, padding=1)
        #https://discuss.pytorch.org/t/convolution-input-and-output-channels/10205
        #http://cs231n.github.io/convolutional-networks/
        # 16 filters that are 3x5x5 summed for each input with a bias of X added at the end
        # convolutional layer equation: ((input_volume - kernel_size + 2 * padding)/stride) + 1
        # take the floor if it is a decimal number
        # explanation in depth
        # input value will be 1x3x40x90
        # there are 16 3x5x5 filter will overlap with 1 input of 3x40x90
        # rgb x height x width
        # 1x5x5 (red) will correspond with 1x40x90 (red)
        # 1x5x5 (green) will correspond with 1x40x90 (green)
        # 1x5x5 (blue) will correspond with 1x40x90 (blue)
        # sum values of convolution created from 1x5x5 (red) with 1x5x5 (green) with 1x5x5 (blue)
        # will equal 1x18x43 (see convolution layer equation) + bias
        # repeat with rest of 15 filters
        # resulting output: 16 x 18 x 43 (16 filter output x reduced height dimension (not actually height anymore) x reduced width dimension (not actually width anymore))
        
        self.conv1 = nn.Conv2d(3, 16, kernel_size=5, stride=2)
        # https://www.aiworkbox.com/lessons/batchnorm2d-how-to-use-the-batchnorm2d-module-in-pytorch
        # https://gist.github.com/shagunsodhani/4441216a298df0fe6ab0
        # https://arxiv.org/abs/1502.03167
        # BatchNorm2d is used to improve the learning rate of a neural network
        # Minimizes internal covariate shift - each layer's input distribution changing as the parameters of the layer above it change during training
        # normalizes the input of each layer to minimize the input distribution change through many iterations. Allows for faster convergence
        self.bn1 = nn.BatchNorm2d(16)
        # 32 filters that are 16x5x5 for each input with bias of x added at the end
        # resulting dimension 32x7x20 = 4480 values
        self.conv2 = nn.Conv2d(16, 32, kernel_size=5, stride=2)
        self.bn2 = nn.BatchNorm2d(32)
        # 32 filters that are 32x5x5 for each input with bias of x added at the end
        # resulting dimension 32x2x8 = 512 values
        self.conv3 = nn.Conv2d(32, 32, kernel_size=5, stride=2)
        self.bn3 = nn.BatchNorm2d(32)
    
        def conv2d_size_out(size, kernel_size=5, stride=2):
            return (size - (kernel_size - 1) - 1) // stride + 1
    
        convw = conv2d_size_out(conv2d_size_out(conv2d_size_out(w)))
        convh = conv2d_size_out(conv2d_size_out(conv2d_size_out(h)))
        linear_input_size = convw * convh * 32
        
        self.head = nn.Linear(linear_input_size, outputs)        
        
        
        
        
    def forward(self,x):
        
        # I am assuming that self.conv1(x) is going through the forward function call in nn.Conv2d
        # random weights are automatically initialized in this situation
        # however, if I want to create weights I would do self.conv1.weight = [pyTorch Tensor]
        # self.conv1(x) is unpacking x into N, C, H, W
        x = F.relu(self.bn1(self.conv1(x)))
        x = F.relu(self.bn2(self.conv2(x)))
        x = F.relu(self.bn3(self.conv3(x)))
        # x.size(0) will return 1
        # the -1 is used to infer what the other dimension will be
        # in this situation there are only 2 actions so -1 will be 2
        # x.view(1,2) will resize the output of self.head to the new torch size of 1x2
        return self.head(x.view(x.size(0), -1))
         
# converting to a pyplot image and resizing the dimensions
# 3x160x360 -> 3x40x90
# the smaller edge of the image (height= 160) will be matched to the resize number (40) and maintaining the aspect ratio
# width > height so => (size, size*width/height) => (40, 40 * 360/160)=>(40, 90)
resize = T.Compose([T.ToPILImage(),
                   T.Resize(40, interpolation=Image.CUBIC),
                   T.ToTensor()])

def get_cart_location(screen_width):
    
    # env.state is an array with the following 4 values: 1-cart_position, 2-cart_velocity, 3-pole_angle, 4-pole_velocity_at_tip
    # the return statement will move of the time return a value that is approximate half of 600 (range: 297-303)
    # the return value is dependent on the current cart position multiplied by the scale and then added to the screen_width.
    
    world_width = env.x_threshold * 2
    scale = screen_width/world_width
    return int(env.state[0] * scale + screen_width / 2.0)

def get_screen():
    # original dimensions were height X width X rgb. Transposing creates the dimenions rgb X height X width
    # the elements in (2,0,1) is the originally corresponding dimension being moved to its new index
    # torch vision uses C x H x W for pytorch and H x W x C for numpy array
    # see ToPILImage
    screen = env.render(mode='rgb_array').transpose((2, 0, 1))
    _, screen_height, screen_width = screen.shape
    
    # reduced the height of the screen to 160. New dimension: 3x160x600
    screen = screen[:, int(screen_height * 0.4):int(screen_height * 0.8)]

    # reduced the width of the screen to 360. new dimension : 3x160x360
    view_width = int(screen_width * 0.6)

    # will generate a number that is approximately half the width of the screen. For instance, if width=600, then cart_location will range from 297-303
    cart_location = get_cart_location(screen_width)

    # My computer results: 
    # view_width//2 => 360/2 = 180, 
    # screen_width - view_width/2 => 600 - 360/2 = 420, 
    # cart_location - view_width/2 => approximate(300) - 360/2 = approximate(120)
    # cart_location + view_width/2 => approximate(300) + 360/2 = approximate(480)
    
    if cart_location < view_width//2:
        slice_range = slice(view_width)
    elif cart_location > (screen_width - view_width // 2):
        slice_range = slice(-view_width, None)
    else:
        slice_range = slice(cart_location - view_width // 2,
                            cart_location + view_width // 2)


    # creates a torch size of 3X160X360 so it will strip off edge and center square image on the cart
    screen = screen[:, :, slice_range]
    screen = np.ascontiguousarray(screen, dtype=np.float32)
    screen = torch.from_numpy(screen)
    
    # converting to a pyplot image and reducing the dimensions by a factor of 40 in the resize
    # 3x160x360 -> 3x40x90
    return resize(screen).unsqueeze(0).to(device)

env.reset()
plt.figure()
plt.imshow(get_screen().cpu().squeeze(0).permute(1, 2, 0).numpy(),
           interpolation='none')
plt.title('Example extracted screen')
plt.show()

BATCH_SIZE = 128
GAMMA = 0.999
EPS_START = 0.9
EPS_END = 0.05
EPS_DECAY = 200
TARGET_UPDATE = 10

init_screen = get_screen()
_, _, screen_height, screen_width = init_screen.shape
n_actions = env.action_space.n
# screen_height:40, screen_width:90
# continue tomorrow inside DQN call to figure out how select action works.
policy_net = DQN(screen_height, screen_width, n_actions).to(device)
target_net = DQN(screen_height, screen_width, n_actions).to(device)

target_net.load_state_dict(policy_net.state_dict())
target_net.eval()

optimizer = optim.RMSprop(policy_net.parameters())
memory = ReplayMemory(10000)

steps_done = 0

def select_action(state):
    global steps_done
    sample = random.random()
    # creates a decay from 0.9 to 0.05
    eps_threshold = EPS_END + (EPS_START - EPS_END) * \
    math.exp(-1 * steps_done / EPS_DECAY)       
    steps_done += 1

    # exploit vs exploration with pyTorch        
    if sample > eps_threshold:
        with torch.no_grad():
            # exploit
            # returns the max value from the output of the policy_net. Reshapes output to 1x1 tensor size with view 
            # note policy_net(state) is calling the forward function in the DQN which is overriding the original model forward function
            return policy_net(state).max(1)[1].view(1,1)
    else:
        # explore
        # returns a torch tensor with a random value that corresponds to the number of actions 0 or 1 in this case.
        return torch.tensor([[random.randrange(n_actions)]], device=device, dtype=torch.long)

episode_durations = []

def plot_durations():
    # creates figure 2
    plt.figure(2)
    # clears the current figure
    plt.clf()
    durations_t = torch.tensor(episode_durations, dtype=torch.float)
    # creates the labels for the plot
    plt.title('Training...')
    plt.xlabel('Episode')
    plt.ylabel('Duration')
    
    # plots a line graph
    plt.plot(durations_t.numpy())
    
    if len(durations_t) >= 100:
        # take every 100 data points 0-99, 1-100, 2-101, etc and create a mean for each and graph it
        # if there are 103 elements then there will be 4 data points
        # 0-99, 1-100, 2-101, 3-102  (remember 0 indexing)
        # unfold will convert into shape [4,100] with each of the first index representing a sequence of 100 elements
        # mean converts to a shape of [4]
        # view -reshapes the tensor. In this case it will be reshaped to [4] still
        means = durations_t.unfold(0, 100, 1).mean(1).view(-1)
        means = torch.cat((torch.zeros(99), means))
        plt.plot(means.numpy())
        
    # pause ensures that graphs will be generated in order?
    plt.pause(0.001)
    if is_ipython:
        display.clear_output(wait=True)
        # get the current figure
        # wont take up memory will just override previous figure
        display.display(plt.gcf())
        
def optimize_model():
    # if there is not enough memory to create a batch size, then do not update weights
    if len(memory) < BATCH_SIZE:
        return
    
    # get BATCH_SIZE sample which in this case is 128
    transitions = memory.sample(BATCH_SIZE)
    # creates a tuple iterator
    # 'state':values, 'action':values, 'next_state':values, 'reward':values
    # this will help with indexing when separating them later
    # index => 5 would correspond to action[5], next_state[5], etc...
    batch = Transition(*zip(*transitions))
    
    # creates a boolean tensor with True/False corresponding to if it was a terminal state or not
    non_final_mask = torch.tensor(tuple(map(lambda s: s is not None,
                                            batch.next_state)), device = device, dtype=torch.bool)

    # captures all the non-final next_state tensor
    non_final_next_states = torch.cat([s for s in batch.next_state if s is not None])
    
    # separates the batch into their own corresponding variable
    state_batch = torch.cat(batch.state)
    action_batch = torch.cat(batch.action)
    reward_batch = torch.cat(batch.reward)
    
    # gathers the numerical value representations that corresponds to the action given the state
    state_action_values = policy_net(state_batch).gather(1, action_batch)
    
    # creates a tensor of 0 values    
    next_state_values = torch.zeros(BATCH_SIZE, device=device)
    
    # creating the Qmax using the next state and picking the max value
    # the 0 values will remain 0
    next_state_values[non_final_mask] = target_net(non_final_next_states).max(1)[0].detach()
    
    # GAMMA * QMAX + reward = Expected
    # will start off as around approximately 1
    expected_state_action_values = (next_state_values * GAMMA) + reward_batch
       
    #loss(expected - actual) = loss resultant
    # the unsqueeze adds an extra dimension at the second index
    # this is to ensure that the dimensions are the same 128x1. Before the unsqueeze the dimension for expected_state_action_values was just 128
    loss = F.smooth_l1_loss(state_action_values, expected_state_action_values.unsqueeze(1)) 
    
    # resets the gradient to zero before calculating
    # there will be scenarios where you won't have to set this to zero such as recurrent neural networks where it requires muliple passes to update
    optimizer.zero_grad()
    # does the backpropagation through the Functional (F) commands
    # note: functional will ask you to define the weights by hand
    # nn.Module is a wrapper around the functional interface
    # calculates/accumulates the gradient
    loss.backward()

    for param in policy_net.parameters():
        # squeezes the data between the two range values
        param.grad.data.clamp_(-1,1)
    # optimizer.step performs a parameter update based on the current gradient -> updating the weight once
    optimizer.step()
    


num_episodes = 500
for i_episode in range(num_episodes):
    env.reset()
    last_screen = get_screen()
    current_screen = get_screen()
    # initial screen value placeholder X RGB X height X width => 1x3x40x90
    state = current_screen - last_screen
    # will keep counting till break
    for t in count():
        action = select_action(state)
        # action.item() will return a python number from a tensor containing a single value. In this case it would be 0 or 1
        # calls the step function in cartpole.py in openAI gym to modify the x and theta values of the simulation
        # reward => reward given for that step
        # done => to check to see if the simulation has ended
        _, reward, done, _ = env.step(action.item())
        # reward of 1 or 0 is converted into torch tensor
        reward = torch.tensor([reward], device=device)
        # updates the frames
        last_screen = current_screen
        current_screen = get_screen()
        if not done:
            next_state = current_screen - last_screen
        else:
            next_state = None
            
        # preserving it into replay_memory
        # state => 1x3x40x90, action => 0 or 1, next_state => 1x3x40x90, reward => 1 or 0
        memory.push(state, action, next_state, reward)

        state = next_state
        # function used to update weights through back-propagation
        optimize_model()
        # used to generate a graph everytime an episode is completed
        if done:
            # appends the x axis by 1
            episode_durations.append(t + 1)
            plot_durations()
            break
    
    # updates the weight of the target_net to match those of the policy_net
    if i_episode % TARGET_UPDATE == 0:
        target_net.load_state_dict(policy_net.state_dict())

print('Complete')
env.render()
# closes the simulation
env.close()
# turns the interactive mode off
plt.ioff()
# displays a figure. Pretty sure it is not needed in this scenario
plt.show()
        
        
        
        
        
        
        
        
        